import './App.css';

const App = () => {
  return (
    <div className="container">
      <div className="row mt-3">
        <div className="col-md-12">
          <h2>CSS Grids</h2>
        </div>
      </div>

      <div className="row mt-3">
        <div className="col-md-12">
          <table class="table table-responsive">
            <thead>
            <tr>
              <th>Heading 1</th>
              <th>Heading 2</th>
              <th>Heading 3</th>
              <th>Heading 4</th>
              <th>Heading 5</th>
              <th>Heading 6</th>
              <th>Heading 7</th>
              <th>Heading 8</th>
              <th>Heading 9</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>2</td>
              <td>3</td>
              <td>4</td>
              <td>5</td>
              <td>6</td>
              <td>7</td>
              <td>8</td>
              <td>Error</td>
              <td>9</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;
